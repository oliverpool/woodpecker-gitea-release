// Copyright 2023 Woodpecker Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gitea_release

import (
	"codeberg.org/woodpecker-plugins/go-plugin"
	"github.com/urfave/cli/v2"
)

type Settings struct {
	APIKey     string
	Files      cli.StringSlice
	FileExists string
	Checksum   cli.StringSlice
	Draft      bool
	PreRelease bool
	BaseURL    string
	Title      string
	Note       string

	uploads []string
}

// Plugin implements provide the plugin implementation.
type Plugin struct {
	*plugin.Plugin
	Settings Settings
	EnvFile  string
}

func New(version string) *Plugin {
	p := &Plugin{}

	p.Plugin = plugin.New(plugin.Options{
		Name:        "plugin-gitea-release",
		Description: "Create a Gitea release",
		Version:     version,
		Flags:       p.Flags(),
		Execute:     p.Execute,
	})

	return p
}

func (p *Plugin) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "api-key",
			Usage:       "api key to access gitea api",
			EnvVars:     []string{"PLUGIN_API_KEY", "GITEA_RELEASE_API_KEY", "GITEA_TOKEN"},
			Destination: &p.Settings.APIKey,
		},
		&cli.StringSliceFlag{
			Name:        "files",
			Usage:       "list of files to upload",
			EnvVars:     []string{"PLUGIN_FILES", "GITEA_RELEASE_FILES"},
			Destination: &p.Settings.Files,
		},
		&cli.StringFlag{
			Name:        "file-exists",
			Value:       "overwrite",
			Usage:       "what to do if file already exist",
			EnvVars:     []string{"PLUGIN_FILE_EXISTS", "GITEA_RELEASE_FILE_EXISTS"},
			Destination: &p.Settings.FileExists,
		},
		&cli.StringSliceFlag{
			Name:        "checksum",
			Usage:       "generate specific checksums",
			EnvVars:     []string{"PLUGIN_CHECKSUM", "GITEA_RELEASE_CHECKSUM"},
			Destination: &p.Settings.Checksum,
		},
		&cli.BoolFlag{
			Name:        "draft",
			Usage:       "create a draft release",
			EnvVars:     []string{"PLUGIN_DRAFT", "GITEA_RELEASE_DRAFT"},
			Destination: &p.Settings.Draft,
		},
		&cli.BoolFlag{
			Name:        "prerelease",
			Usage:       "set the release as prerelease",
			EnvVars:     []string{"PLUGIN_PRERELEASE", "GITEA_RELEASE_PRERELEASE"},
			Destination: &p.Settings.PreRelease,
		},
		&cli.StringFlag{
			Name:        "base-url",
			Usage:       "url of the gitea instance",
			EnvVars:     []string{"PLUGIN_BASE_URL", "GITEA_RELEASE_BASE_URL"},
			Destination: &p.Settings.BaseURL,
		},
		&cli.StringFlag{
			Name:        "note",
			Value:       "",
			Usage:       "file or string with notes for the release (example: changelog)",
			EnvVars:     []string{"PLUGIN_NOTE", "GITEA_RELEASE_NOTE"},
			Destination: &p.Settings.Note,
		},
		&cli.StringFlag{
			Name:        "title",
			Value:       "",
			Usage:       "file or string for the title shown in the gitea release",
			EnvVars:     []string{"PLUGIN_TITLE", "GITEA_RELEASE_TITLE"},
			Destination: &p.Settings.Title,
		},
		&cli.StringFlag{
			Name:        "env-file",
			Usage:       "source env file",
			Destination: &p.EnvFile,
		},
	}
}
