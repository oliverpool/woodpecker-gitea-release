// Copyright 2023 Woodpecker Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gitea_release

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/joho/godotenv"
)

func (p *Plugin) Validate() error {
	if p.Metadata.Pipeline.Event != "tag" {
		return fmt.Errorf("the gitea-release plugin is only available for tags")
	}

	if p.Settings.APIKey == "" {
		return fmt.Errorf("you must provide an API key")
	}

	if !fileExistsValues[p.Settings.FileExists] {
		return fmt.Errorf("invalid value for file_exists")
	}

	if p.Settings.BaseURL == "" {
		return fmt.Errorf("you must provide a base url.")
	}

	if !strings.HasSuffix(p.Settings.BaseURL, "/") {
		p.Settings.BaseURL += "/"
	}

	var err error
	if p.Settings.Note != "" {
		if p.Settings.Note, err = readStringOrFile(p.Settings.Note); err != nil {
			return fmt.Errorf("error while reading %s: %v", p.Settings.Note, err)
		}
	}

	if p.Settings.Title != "" {
		if p.Settings.Title, err = readStringOrFile(p.Settings.Title); err != nil {
			return fmt.Errorf("error while reading %s: %v", p.Settings.Note, err)
		}
	}

	var files []string
	for _, glob := range p.Settings.Files.Value() {
		globbed, err := filepath.Glob(glob)
		if err != nil {
			return fmt.Errorf("failed to glob %s. %s", glob, err)
		}

		if globbed != nil {
			files = append(files, globbed...)
		}
	}

	if len(p.Settings.Checksum.Value()) > 0 {
		var err error

		files, err = writeChecksums(files, p.Settings.Checksum.Value())

		if err != nil {
			return fmt.Errorf("failed to write checksums. %v", err)
		}
	}
	p.Settings.uploads = files

	return nil
}

func (p *Plugin) Execute(ctx context.Context) error {
	if p.EnvFile != "" {
		if err := godotenv.Overload(p.EnvFile); err != nil {
			return err
		}
	}

	if err := p.Validate(); err != nil {
		return err
	}

	client, err := gitea.NewClient(p.Settings.BaseURL, gitea.SetToken(p.Settings.APIKey), gitea.SetHTTPClient(p.HTTPClient()), gitea.SetContext(ctx))
	if err != nil {
		return err
	}

	rc := releaseClient{
		Client:     client,
		Owner:      p.Metadata.Repository.Owner,
		Repo:       p.Metadata.Repository.Name,
		Tag:        strings.TrimPrefix(p.Metadata.Curr.Ref, "refs/tags/"),
		Draft:      p.Settings.Draft,
		Prerelease: p.Settings.PreRelease,
		FileExists: p.Settings.FileExists,
		Title:      p.Settings.Title,
		Note:       p.Settings.Note,
	}

	// if the title was not provided via .drone.yml we use the tag instead
	// fixes https://github.com/drone-plugins/drone-gitea-release/issues/26
	if rc.Title == "" {
		rc.Title = rc.Tag
	}

	release, err := rc.buildRelease()
	if err != nil {
		return fmt.Errorf("Failed to create the release. %s", err)
	}

	if err := rc.uploadFiles(release.ID, p.Settings.uploads); err != nil {
		return fmt.Errorf("failed to upload the files. %v", err)
	}

	return nil
}

func readStringOrFile(input string) (string, error) {
	// Check if input is a file path
	if _, err := os.Stat(input); err != nil && os.IsNotExist(err) {
		// No file found => use input as result
		return input, nil
	} else if err != nil {
		return "", err
	}
	result, err := os.ReadFile(input)
	if err != nil {
		return "", err
	}
	return string(result), nil
}
