module codeberg.org/woodpecker-plugins/plugin-gitea-release

go 1.19

require (
	code.gitea.io/sdk/gitea v0.15.1
	codeberg.org/woodpecker-plugins/go-plugin v0.2.0
	github.com/joho/godotenv v1.4.0
	github.com/rs/zerolog v1.28.0
	github.com/urfave/cli/v2 v2.23.7
	golang.org/x/crypto v0.4.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
)
