# plugin-gitea-release

<a href="https://codeberg.org/woodpecker-plugins/plugin-gitea-release">
  <img alt="Get it on Codeberg" src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-neon-blue.png" height="60">
</a>


Woodpecker CI plugin to publish files and artifacts to Gitea release. For the usage information and a listing of the available options please take a look at [the docs](docs.md) or [woodpecker-ci.org](https://woodpecker-ci.org/plugins/Gitea%20Release).

This plugin is a fork of [drone-gitea-release](https://github.com/drone-plugins/drone-gitea-release).

## Build

Build the binary with the following command:

```console
make build
```

## Docker

Build the Docker image with the following command:

```console
docker buildx build \
  --label org.label-schema.build-date=$(date -u +"%Y-%m-%dT%H:%M:%SZ") \
  --label org.label-schema.vcs-ref=$(git rev-parse --short HEAD) \
  --platform linux/amd64 \
  --file Dockerfile.multiarch --tag woodpeckerci/plugin-gitea-release .
```

## Contributors

Special thanks goes to all [contributors](https://codeberg.org/woodpecker-plugins/plugin-gitea-release/activity). If you would like to contribute,
please see the [instructions](https://codeberg.org/woodpecker-plugins/plugin-gitea-release/src/branch/main/CONTRIBUTING.md).

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](https://codeberg.org/woodpecker-plugins/plugin-gitea-release/src/branch/main/LICENSE) file for details.
