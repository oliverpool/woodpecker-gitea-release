---
name: Gitea Release
icon: https://raw.githubusercontent.com/go-gitea/gitea/main/assets/logo.svg
description: Plugin to create a Gitea release
authors: Woodpecker Authors
tags: [Gitea, publish]
containerImage: woodpeckerci/plugin-gitea-release
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-gitea-release
url: https://codeberg.org/woodpecker-plugins/gitea-release
---

Woodpecker CI plugin to create a Gitea release. This plugin is a fork of [drone-gitea-release](https://github.com/drone-plugins/drone-gitea-release).

## Settings

| Settings Name | Default     | Description                                                              |
|---------------|-------------|--------------------------------------------------------------------------|
| `api-key`     | *none*      | API key for Gitea                                                        |
| `files`       | *none*      | List of files to upload                                                  |
| `file-exists` | `overwrite` | What to do if files already exist; one of `overwrite`, `fail`, or `skip` |
| `checksum`    | *none*      | Generate checksums for specific files                                    |
| `draft`       | `false`     | Create a draft release                                                   |
| `skip-verify` | `false`     | Visit `base-url` and skip verifying certificate                          |
| `prerelease`  | `false`     | Create a pre-release                                                     |
| `base-url`    | *none*      | Base URL of Gitea instance                                               |
| `note`        | *none*      | File or string with notes for the release (ex: changelog)                |
| `title`       | *none*      | File or string with the title for the release                            |
| `env-file`    | *none*      | Path to a `.env` file to load                                            |

## Example

```yaml
  publish:
    image: woodpeckerci/plugin-gitea-release
    settings:
        base_url: https://codeberg.org
        files:
          - "hello-world"
          - "hello-world.exe"
        api_key:
          from_secret: API_KEY
```
